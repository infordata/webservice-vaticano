<?php

$cmd = $_POST['cmd'];



/*
 * Array da modificare per aggiungere nuovi utenti
 * La chiave è la codifica esadecimale del UID
 */
$users = array(
    '8a6cd130' => array(
        'utente_nome' => 'FRANCESCO',
        'uid_tag' => '2322387248',
        'stato_tessera' => 'V',
        'attenzione' => false,
        'note' => '',
        'url_foto' => "http://{$_SERVER['HTTP_HOST']}/15.jpg"
    ),
    'a255b9bd' => array(
        'utente_nome' => 'Anna',
        'uid_tag' => '2322387248',
        'stato_tessera' => 'V',
        'attenzione' => false,
        'note' => 'Tutto a posto',
        'url_foto' => "http://{$_SERVER['HTTP_HOST']}/1.jpg"
    ),
    '046ef89a983c80' => array(
        'utente_nome' => 'Mario Rossi',
        'uid_tag' => '2322387248',
        'stato_tessera' => 'S',
        'attenzione' => false,
        'note' => 'Attenzione, questo utente ha smarrito la tessera.',
        'url_foto' => "http://{$_SERVER['HTTP_HOST']}/2.jpg"
    ),
    'c6540891' => array(
        'utente_nome' => 'UTENTE CON UN NOME ABBASTANZA LUNGO',
        'uid_tag' => '2322387248',
        'stato_tessera' => 'V',
        'attenzione' => true,
        'note' => 'In questo caso non viene visualizzato lo stato perché è attivo il flag di attenzione',
        'url_foto' => "http://{$_SERVER['HTTP_HOST']}/3.jpg"
    ),
    );

if($cmd == 'get_user')  {

    $dati = json_decode($_REQUEST['data']);
    echo json_encode(array(
                            'success'=>true,
                            'data'=>array($users[$dati->uid_tag]))
                    );
}


if($cmd == 'get_config')
    echo '{"success":true,"data":[{"passback_interval":5}]}';
elseif($cmd == 'heartbeat')
    //echo '{"success":true,"data"=>[{"rpc"=>""}]}';
    echo '{"success":true}';



?>

